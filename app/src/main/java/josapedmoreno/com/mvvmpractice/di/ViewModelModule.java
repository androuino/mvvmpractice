package josapedmoreno.com.mvvmpractice.di;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;
import josapedmoreno.com.mvvmpractice.ui.UserViewModel;
import josapedmoreno.com.mvvmpractice.viewmodel.ViewModelFactory;

/**
 * Created by sem on 2/9/18.
 *
 */

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(UserViewModel.class)
    abstract ViewModel bindUserViewModel(UserViewModel userViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);
}
