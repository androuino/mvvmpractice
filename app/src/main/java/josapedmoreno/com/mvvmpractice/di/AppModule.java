package josapedmoreno.com.mvvmpractice.di;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import josapedmoreno.com.mvvmpractice.logic.UserOperations;
import josapedmoreno.com.mvvmpractice.logic.UserSettings;

/**
 * Created by sem on 2/9/18.
 *
 */
@Module(includes = ViewModelModule.class)
class AppModule {
    @Singleton @Provides
    UserSettings provideUserSettings() {
        return new UserSettings();
    }

    @Singleton @Provides
    UserOperations provideUserOperations() {
        return new UserOperations(provideUserSettings());
    }
}
