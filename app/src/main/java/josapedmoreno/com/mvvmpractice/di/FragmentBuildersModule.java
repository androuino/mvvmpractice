package josapedmoreno.com.mvvmpractice.di;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import josapedmoreno.com.mvvmpractice.ui.UserFragment;

/**
 * Created by sem on 2/9/18.
 *
 */
@Module
public abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract UserFragment contributeUserFragment();
}
