package josapedmoreno.com.mvvmpractice.di;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import josapedmoreno.com.mvvmpractice.MainActivity;

/**
 * Created by sem on 2/9/18.
 *
 */
@Module
public abstract class MainActivityModule {
    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract MainActivity contributeMainActivity();
}
