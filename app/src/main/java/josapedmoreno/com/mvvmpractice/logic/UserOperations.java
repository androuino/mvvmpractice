package josapedmoreno.com.mvvmpractice.logic;

/**
 * Created by sem on 2/9/18.
 *
 */
public class UserOperations {
    private UserSettings userSettings;

    public UserOperations(UserSettings userSettings) {
        this.userSettings = userSettings;
    }

    public String displayUserSetting() {
        return userSettings.getName();
    }
}
