package josapedmoreno.com.mvvmpractice.logic;


/**
 * Created by sem on 2/9/18.
 *
 */
public class UserSettings implements UserSettingsInterface {
    private String name = "Bar Foo";

    String getName() {
        return name;
    }

    @Override
    public String getAddress(String address) {
        return address;
    }
}
