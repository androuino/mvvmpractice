package josapedmoreno.com.mvvmpractice;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import javax.inject.Inject;

import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import josapedmoreno.com.mvvmpractice.logic.UserOperations;
import josapedmoreno.com.mvvmpractice.ui.common.NavigationController;

public class MainActivity extends AppCompatActivity implements HasSupportFragmentInjector {
    //private UserViewModel userViewModel;
    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;
    @Inject
    NavigationController navigationController;
    @Inject
    UserOperations userOperations;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        //userViewModel.displayUserSetting();
        if (savedInstanceState == null) {
            navigationController.navigateToUser();
        }
        if (userOperations == null) {
            Log.v("UserOperation", "null!");
        } else {
            Log.v("UserOperation", userOperations.displayUserSetting());
        }
    }

    @Override
    public DispatchingAndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }
}
