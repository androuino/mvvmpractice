package josapedmoreno.com.mvvmpractice.ui;

import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import javax.inject.Inject;

import josapedmoreno.com.mvvmpractice.logic.UserOperations;

/**
 * Created by sem on 2/9/18.
 *
 */

public class UserViewModel extends ViewModel implements LifecycleObserver {
    String setting;
    private final MutableLiveData<String> us = new MutableLiveData<>();
    @Inject
    UserOperations userOperations;

    @Inject
    UserViewModel() {
        setting = userOperations.displayUserSetting();
        us.setValue(setting);
    }

    public LiveData<String> displayUserSetting() {
        return us;
    }
}
