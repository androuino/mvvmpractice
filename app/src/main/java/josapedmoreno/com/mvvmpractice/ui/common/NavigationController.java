package josapedmoreno.com.mvvmpractice.ui.common;

import android.support.v4.app.FragmentManager;

import javax.inject.Inject;

import josapedmoreno.com.mvvmpractice.MainActivity;
import josapedmoreno.com.mvvmpractice.R;
import josapedmoreno.com.mvvmpractice.ui.UserFragment;

/**
 * Created by sem on 2/13/18.
 *
 * A utility class that handles navigation in {@link MainActivity}.
 */

public class NavigationController {
    private final int containerId;
    private final FragmentManager fragmentManager;
    @Inject
    public NavigationController(MainActivity mainActivity) {
        this.containerId = R.id.container;
        this.fragmentManager = mainActivity.getSupportFragmentManager();
    }

    public void navigateToUser() {
        UserFragment userFragment = new UserFragment();
        fragmentManager.beginTransaction()
                .replace(containerId, userFragment)
                .commitAllowingStateLoss();
    }
}
